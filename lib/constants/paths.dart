class AppPaths {
  static final String defaultProfilePicture =
      'lib/assets/images/default_account.png';
  static final String customAppMarker = 'lib/assets/images/safeME-pin2.png';
  static final String appLogo = 'lib/assets/images/safeME.png';
}
